<?php include("header.php");?>
	<section class="module back-patch">
		<div class="container">
			<article class="main-article">
				<header class="article-header">
					<div class="breadcrumb-block">
						<a href=""><span class="breadcrumb">Todas las noticias ></span></a>
					</div>
					<h1 class="title">Partido de hoy 12 de octubre de la Copa Águila</h1>
					<div class="share">Compartir <a href=""><span class="icon icon-facebook"></span></a> <a href=""><span class="icon icon-twitter"></span></a></div>
				</header>
				<section class="article-content">
					<div class="left-block">
						<figure>
							<img src="img/news/figure01.png" alt="">
							<figcaption>Jueves, 09 de julio 2016</figcaption>
						</figure>
						<p><b><i>Por: Samuel Rios</i></b></p>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eleifend orci ac suscipit maximus. Suspendisse a tincidunt turpis, sed imperdiet tellus. Mauris consectetur sapien sit amet placerat facilisis. Nulla velit metus, mattis eget pharetra sit amet, eleifend eu tellus. Proin at magna eu metus elementum pellentesque ut ut orci. Duis feugiat odio eu pretium pretium. Sed faucibus lacus non suscipit congue. Pellentesque interdum laoreet orci in lobortis.</p>
						<p>Donec ornare luctus dignissim. Nunc in aliquam neque. Suspendisse ante massa, ultricies sit amet cursus sodales, blandit placerat erat. In bibendum nulla lectus. Nullam non rhoncus urna. Suspendisse ac diam euismod, faucibus mauris at, tristique est. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed quis ex eget neque rutrum pulvinar. Curabitur sagittis metus quis tortor pretium, placerat aliquam augue pellentesque. Aliquam accumsan nisl quis maximus pellentesque. Maecenas venenatis nisl at justo commodo, at semper libero aliquam. Integer scelerisque eu dolor non suscipit.</p>
						<figure class="cover-video">
							<iframe src="https://www.youtube.com/embed/wegXKG-hhF0" frameborder="0" allowfullscreen></iframe>
						</figure>
						<img src="img/news/figure02.png" alt="" class="align-center">
						<p>Proin sagittis est diam, eu ultrices dui molestie ultricies. Vivamus lacus lectus, vehicula vitae sagittis quis, tempor eleifend lacus. Proin sit amet porttitor dolor. Integer aliquam, lectus in fringilla tristique, nibh nisi cursus neque, in bibendum urna nulla vitae nisl. Curabitur id scelerisque urna. Etiam euis mod mollis orci at cursus. Ut finibus at ante ac fringilla. Sed vel enim condimentum, posuere dolor sed, auctor enim. Aliquam condimentum fermentum dui, ut pellentesque orci ornare sed. Donec euismod porta quam.</p>
						<img src="img/news/figure02.png" alt="" class="align-left">
						<p>Phasellus lectus ex, ultrices sit amet consequat vitae, accumsan eget velit. Nam dignissim nibh a ex volutpat, et lacinia elit finibus. Curabitur euismod, leo at tempor sollicitudin, diam neque ornare eros, vitae pellentesque tellus tellus eget quam. Quisque eu lectus in libero eleifend condimentum. Aliquam euismod cursus ultrices. Mauris suscipit velit sit amet efficitur tristique. Pellentesque ex massa, tristique id eros sed, ultrices condimentum nibh. Aliquam sapien nisi, semper tempor imperdiet eget, dapibus id massa.</p>
						<img src="img/news/figure02.png" alt="" class="align-right">
						<p>Etiam pulvinar iaculis sapien. Phasellus porttitor erat vitae enim tincidunt, nec dictum ex euismod. Aliquam lacus eros, gravida vitae justo quis, gravida ullamcorper neque. Nunc sed convallis metus, a ultrices arcu. Vivamus iaculis nec quam in ullamcorper. Donec mollis nisi felis. Sed aliquet ex venenatis nulla dictum sollicitudin.</p>
						<p>Cras efficitur faucibus porta. Pellentesque enim ligula, tempus at arcu ut, tempor volutpat nunc. Donec tortor arcu, dapibus ut lacus ut, posuere venenatis enim. Maecenas non nunc pretium, fermentum lorem sed, rhoncus odio. Vestibulum sed augue felis. Aenean consectetur eros et ex finibus ultricies. Fusce quis dictum diam. Aliquam tincidunt aliquam enim sit amet facilisis. Vivamus quis orci euismod sem efficitur dignissim. Phasellus blandit dignissim turpis, a pellentesque libero pellentesque sit amet.</p>
						<p>Aliquam scelerisque bibendum ligula sit amet egestas. Cras tempus, erat ut pretium semper, erat lectus congue turpis, et ornare tellus nisi at nisi. Donec iaculis maximus molestie. Aliquam sodales, ante ac porta laoreet, est leo condimentum metus, ut lobortis enim magna non felis. Maecenas id enim aliquet leo sollicitudin posuere. Praesent vitae dui in magna varius consequat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla in sapien quis turpis volutpat rhoncus. Integer cursus congue nunc, in gravida nisi lobortis nec. Ut dictum elementum purus, molestie placerat magna pretium non. Integer ex nisl, dapibus eget malesuada quis, iaculis at orci.</p>
						<div class="share">Comparte este contenido en <a href=""><span class="icon icon-facebook"></span></a> <a href=""><span class="icon icon-twitter"></span></a></div>
					</div>
					<aside class="publish-block">
						<div class="block-img"><img src="img/publish02.png" alt=""></div>
						<div class="more-news">
							<p class="title">No te pierdas</p>
							<div class="news-block">
								<a href=""><span class="link"></span></a>
								<figure class="cover-img">
									<img src="img/news/img01.png" alt="">
									<time>27 de Julio de 2016</time>
								</figure>
								<div class="cover-text">
									<ul class="tag">
										<li>Categoria</li>
									</ul>
									<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
								</div>
							</div>
							<div class="news-block">
								<a href=""><span class="link"></span></a>
								<figure class="cover-img">
									<img src="img/news/img01.png" alt="">
									<time>27 de Julio de 2016</time>
								</figure>
								<div class="cover-text">
									<ul class="tag">
										<li>Categoria</li>
									</ul>
									<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
								</div>
							</div>
							<div class="news-block">
								<a href=""><span class="link"></span></a>
								<figure class="cover-img">
									<img src="img/news/img01.png" alt="">
									<time>27 de Julio de 2016</time>
								</figure>
								<div class="cover-text">
									<ul class="tag">
										<li>Categoria</li>
									</ul>
									<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
								</div>
							</div>
						</div>
						<div class="box-first-multimedia">
							<img src="img/content/content-02.jpg" alt="">
							<div class="cover-image">
								<div class="text-multimedia">
									<h2>Lorem ipsum dolor sitamet, consectetur adipisicing elit.</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nisi mauris, viverra a dolor id, sodales aliquam metus.</p>
								</div>
							</div>
						</div>
					</aside>
				</section>
			</article>
		</div>
	</section>
	<section class="module backgray">
		<div class="container">
			<h2 class="title">Podría interesarte</h2>
			<div class="more-news">
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
			</div>
			<div class="text-center">
				<a href=""><span class="especial-btn red">MÁS NOTICIAS</span></a>
			</div>
		</div>
	</section>
<?php include("footer.php");?>