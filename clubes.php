<?php include("header.php");?>
	<section class="module back-patch">
		<div class="container">
			<h1 class="title">Clubes</h1>
			<div class="clearfix">
				<div class="left-block">
					<div class="clubes-block">
						<div class="club blue">
							<figure class="cover-img">
								<img src="img/clubes/stadium01.png" alt="">
								<figcaption><img src="img/clubes/shield01.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Deportivo Cali</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club red">
							<figure class="cover-img">
								<img src="img/clubes/stadium02.png" alt="">
								<figcaption><img src="img/clubes/shield02.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Atlético Bucaramanga</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club yellow">
							<figure class="cover-img">
								<img src="img/clubes/stadium03.png" alt="">
								<figcaption><img src="img/clubes/shield03.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Aguila Doradas</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club green">
							<figure class="cover-img">
								<img src="img/clubes/stadium04.png" alt="">
								<figcaption><img src="img/clubes/shield04.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Alianza Petrolera</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club blue">
							<figure class="cover-img">
								<img src="img/clubes/stadium05.png" alt="">
								<figcaption><img src="img/clubes/shield05.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">América de Cali</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club red">
							<figure class="cover-img">
								<img src="img/clubes/stadium06.png" alt="">
								<figcaption><img src="img/clubes/shield06.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Atlético Fútbol Club</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club yellow">
							<figure class="cover-img">
								<img src="img/clubes/stadium07.png" alt="">
								<figcaption><img src="img/clubes/shield07.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Boyacá Chicó </h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club green">
							<figure class="cover-img">
								<img src="img/clubes/stadium08.png" alt="">
								<figcaption><img src="img/clubes/shield08.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">B/quilla Fútbol Club</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club blue">
							<figure class="cover-img">
								<img src="img/clubes/stadium09.png" alt="">
								<figcaption><img src="img/clubes/shield09.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Bogotá Fútbol Club</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club red">
							<figure class="cover-img">
								<img src="img/clubes/stadium10.png" alt="">
								<figcaption><img src="img/clubes/shield10.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Real Cartagena</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club yellow">
							<figure class="cover-img">
								<img src="img/clubes/stadium11.png" alt="">
								<figcaption><img src="img/clubes/shield11.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Cortuluá</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club green">
							<figure class="cover-img">
								<img src="img/clubes/stadium12.png" alt="">
								<figcaption><img src="img/clubes/shield12.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Cúcuta Deportivo</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club blue">
							<figure class="cover-img">
								<img src="img/clubes/stadium13.png" alt="">
								<figcaption><img src="img/clubes/shield13.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Once Caldas</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club red">
							<figure class="cover-img">
								<img src="img/clubes/stadium14.png" alt="">
								<figcaption><img src="img/clubes/shield14.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Orsomarso S.C.</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club yellow">
							<figure class="cover-img">
								<img src="img/clubes/stadium15.png" alt="">
								<figcaption><img src="img/clubes/shield15.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Deportivo Pasto</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div><!--
						--><div class="club green">
							<figure class="cover-img">
								<img src="img/clubes/stadium16.png" alt="">
								<figcaption><img src="img/clubes/shield16.png" alt=""></figcaption>
								<a href="clubes-detalle.php"><span class="link"></span></a>
							</figure>
							<div class="cover-text">
								<h2 class="name">Patriotas Boyacá</h2>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
									<li><a href=""><span class="icon icon-www"></span></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<aside class="publish-block">
					<div class="ranking-widget">
						<script type="text/javascript">
							(function( ) {
								var p = {
									height: 426,
									width: 296,
									id: 'widget-ds-iframe-dim34E2G4RWS',
									theme: 'dimayor',
									opts: 'posiciones'
								};
								document.write(['<iframe id="', p.id,'" style="','border:0;','width:',(p.width?Math.max(280,p.width)+'px;':'100%;'),'height:',Math.max(400,p.height),'px"',' scrolling="no"',' frameborder="0"',' height="',Math.max(400,p.height),'" width="',(p.width?Math.max(280,p.width):'100%'),'" allowtransparency="true"', ' src="//widgets.dayscript.com/clients/v01/?',(function(){var a = [];p.height=(p.height?Math.max(400,p.height):'');p.width=(p.width?Math.max(280,p.width):'');p.rand=(Math.random()*1000);for(var s in p){a.push(s+'='+p[s])};return a.join('&')})(),'"><\/iframe>'].join(''));
							})();
						</script>
					</div>
					<div class="block-img"><img src="img/advertising.png" alt=""></div>

				</aside>
			</div>
		</div>
	</section>
<?php include("footer.php");?>