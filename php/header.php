<!DOCTYPE html>
<html lang="es" xml:lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Dimayor</title>
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, minimum-scale=1.0">
	<link rel="icon" href="/favicon.ico">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css?v=2.5">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/other-styles.css?v=2.6">
</head>
<body>
	<header>
		<div class="principal-header">
			<div class="top-header">
				<div class="container clearfix ph">
					<div class="text-clubes lg_1 md_1 sm_1">
						<p>Clubes</p>
					</div>
					<div class="gallery-clubes lg_11 md_11 sm_11">
						<?php query_posts('post_type=club&posts_per_page=60');
            while ( have_posts() ){ the_post();
            if(get_post_meta(get_the_ID(),'clubcarrusel', true)){
            $foto=wp_get_attachment_image_src(get_post_meta(get_the_ID(),'clubcarrusel', true),'fotoclub'); 
             $id = get_the_ID();
             if($id==80) $foto[0]='http://dimayor.com.co/wp-content/uploads/2015/01/Patriotas-nuevo1-e1422656625989.png';
            ?>
            <div class="slides">
							<a href="<?php the_permalink();?>" ><img src="<?=$foto[0]; ?>" alt=""></a>
							<div class="label-club">
								<span>Club 01</span>
							</div>
						</div>
            <?php } 
            }
            wp_reset_query();
            ?>
            <!--
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-02.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-03.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-04.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-05.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-06.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-07.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-08.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-09.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-10.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-11.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-12.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-13.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-14.png" alt="">
						</div>
						<div class="slides">
							<img src="<?php echo get_template_directory_uri(); ?>/img/clubes/club-color/club-15.png" alt="">
						</div>
            -->
					</div>
				</div>
			</div>
      
			<div class="bot-header">
				<div class="container">
					<div class="ctrl-menu">
						<span class="span1"></span>
						<span class="span2"></span>
						<span class="span3"></span>
					</div>
					<div class="principal-logo">
						<a href="http://dimayor.com.co/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt=""></a>
					</div>
					<!--
          <nav>
						<ul>
							<li class="logo"><a href="/"><span></span></a><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt=""></li>
							<li class="sub" data-focus="submenu-clubes">Clubes
								<div class="submenu clubes">
									<div class="container">
										<ul>
											<li><a href="/category/club-liga-aguila/"><span></span></a>Liga Águila</li>
											<li><a href="/category/club-torneo-aguila/"><span></span></a>Torneo Águila</li>
											<li><a href="/cuadro-de-campeones/"><span></span></a>Cuadro campeones</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="sub" data-focus="submenu-campeones">Campeonatos
								<div class="submenu campeonatos">
									<div class="container">
										<ul>
											<li><a class="active" href="/sistema"><span></span></a>Liga Águila</li>
											<li><a href="/sistema-torneo-aguila/"><span></span></a>Torneo Águila</li>
											<li><a href="/sistema-copa-aguila/"><span></span></a>Copa Águila</li>
											<li><a href="/category/superliga/"><span></span></a>Super Liga Águila</li>
										</ul>
									</div>
								</div>
							</li>
							<li><a href="/category/calendario-dimayor/"><span></span></a>Calendario</li>

							<li class="sub">Noticias
								<div class="submenu noticias">
									<div class="container">
										<ul>
											<li><a href="/category/noticias/"><span></span></a>Noticias</li>									
											<li><a href="/category/por-los-clubes/"><span></span></a>Por los clubes</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="sub">Resoluciones
								<div class="submenu resoluciones">
									<div class="container">
										<ul>
											<li><a href="/category/resoluciones/cd-campeonato/"><span></span></a>CD del campeonatos</li>
											<li><a href="/category/resoluciones/cd-dimayor/"><span></span></a>CD cd la Dimayor</li>
											<li><a href="/category/resoluciones/comision-estatuto-jugador/"><span></span></a>Comisión Estatuto Jugador</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="sub">La Dimayor
								<div class="submenu dimayor">
									<div class="container">
										<ul>
											<li><a href="/category/rinconhistorico/"><span></span></a>Rincón historico</li>
											<li><a href="/opinion/"><span></span></a>Opiniones</li>
										</ul>
									</div>
								</div>
							</li>
              <li class="sub">Responsabilidad social
								<div class="submenu dimayor">
									<div class="container">
										<ul>											
											<li><a href="/category/somos-familia/"><span></span></a>Somos Familia</li>
										</ul>
									</div>
								</div>
							</li>
							             
						</ul>
					</nav>-->
          
          <nav>
    				<ul>
              <li class="logo"><a href="http://dimayor.com.co/"><img src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt=""></a></li>
              <?php //$itemsMenu = wp_get_nav_menu_items('Menú 2016');                        
              wp_nav_menu( array( 'theme_location' => 'primary','container'=>false,'link_before'=>'<span>','link_after'=>'</span>', 'menu_class' => 'nav-menu','items_wrap' => '%3$s' ) ); ?>
            </ul>
          </nav>
          
					<div class="btn-search">
						<span class="icon icon-search"></span>
					</div>
				</div>
			</div>
			<div class="search-bar">
				<div class="container">
					<span class="icon icon-search"></span>
					<form action="/">
						<input type="text" name="s" id="s" value="" placeholder="¿Qué buscas?">
					</form>
				</div>
			</div>
		</div>
	</header>
	<div class="wrapper">