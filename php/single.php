<?php get_header(); ?>
	<section class="module back-patch">
		<div class="container">
			<article class="main-article">
				<header class="article-header">
					<div class="breadcrumb-block">
						<a href=""><span class="breadcrumb">Todas las noticias ></span></a>
					</div>
					<h1 class="title"><?php the_title();?></h1>
					<?php 
            $url_articulo = get_permalink();			
    			  $acortar_enlace = file_get_contents('http://tyk.fm/url_shortener.php?link=' . $url_articulo, true);
    			  $pos = strpos($acortar_enlace, "||");
    			  if ($pos !== false)
    			  {
    			    list($resultado_proceso, $url_corta) = explode("||", $acortar_enlace);
      				if ($resultado_proceso == 'ok')
      				  $url_corta_final = $url_corta;
      				else
      				  $url_corta_final = $url_articulo;
    			  }
            $fraseTwitter = get_the_title();
            ?>
          <div class="share">Compartir <a href="http://www.facebook.com/share.php?u=<?php echo $url_corta_final;?>" target="_blank"><span class="icon icon-facebook"></span></a> 
          <a href="http://twitter.com/home?status=<?php echo urlencode($fraseTwitter).' '.$url_corta_final; ?>" target="_blank" ><span class="icon icon-twitter"></span></a></div>
				</header>
				<section class="article-content">
					<?php while ( have_posts() ) { the_post(); ?>
          <div class="left-block">
						<figure>							
              <?php the_post_thumbnail('portada'); ?>
							<figcaption><?php the_date('l, j \d\e F , Y');?></figcaption>
						</figure>
						<p><b><i>Por: <?php the_author_posts_link() ?></i></b></p>
						<?php the_content(); ?>			
            <?php if(get_post_meta( get_the_ID(), 'dimayor_archivo_pdf', true)){?>
             <p><!--<a href="<?php echo wp_get_attachment_url( get_post_meta( get_the_ID(), 'dimayor_archivo_pdf', true)); ?>"><span class="btn">DESCARGAR <i class="icon icon-download"></i></span></a>-->
            <embed src="<?php echo wp_get_attachment_url( get_post_meta( get_the_ID(), 'dimayor_archivo_pdf', true)); ?>#toolbar=0"  width="100%" height="1000px" align="center">
            </p>  
            <?php } ?>			
            <div class="share">Comparte este contenido en <a href="http://www.facebook.com/share.php?u=<?php echo $url_corta_final;?>" target="_blank"><span class="icon icon-facebook"></span></a> 
            <a href="http://twitter.com/home?status=<?php echo urlencode($fraseTwitter).' '.$url_corta_final; ?>" target="_blank" ><span class="icon icon-twitter"></span></a></div>
					</div>
          <?php }?>
					
          <aside class="publish-block">
          <?php get_sidebar(); ?>
          </aside>
          <!--
          <aside class="publish-block">
						<div class="block-img"><img src="<?php echo get_template_directory_uri(); ?>/img/publish02.png" alt=""></div>
						<div class="more-news">
							<p class="title">No te pierdas</p>
							<div class="news-block">
								<a href=""><span class="link"></span></a>
								<figure class="cover-img">
									<img src="<?php echo get_template_directory_uri(); ?>/img/news/img01.png" alt="">
									<time>27 de Julio de 2016</time>
								</figure>
								<div class="cover-text">
									<ul class="tag">
										<li>Categoria</li>
									</ul>
									<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
								</div>
							</div>
							<div class="news-block">
								<a href=""><span class="link"></span></a>
								<figure class="cover-img">
									<img src="<?php echo get_template_directory_uri(); ?>/img/news/img01.png" alt="">
									<time>27 de Julio de 2016</time>
								</figure>
								<div class="cover-text">
									<ul class="tag">
										<li>Categoria</li>
									</ul>
									<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
								</div>
							</div>
							<div class="news-block">
								<a href=""><span class="link"></span></a>
								<figure class="cover-img">
									<img src="<?php echo get_template_directory_uri(); ?>/img/news/img01.png" alt="">
									<time>27 de Julio de 2016</time>
								</figure>
								<div class="cover-text">
									<ul class="tag">
										<li>Categoria</li>
									</ul>
									<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
								</div>
							</div>
						</div>
						<div class="box-first-multimedia">
							<img src="<?php echo get_template_directory_uri(); ?>/img/content/content-02.jpg" alt="">
							<div class="cover-image">
								<div class="text-multimedia">
									<h2>Lorem ipsum dolor sitamet, consectetur adipisicing elit.</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nisi mauris, viverra a dolor id, sodales aliquam metus.</p>
								</div>
							</div>
						</div>
					</aside>
          -->
				</section>
			</article>
		</div>
	</section>
	<!--
  <section class="module backgray">
		<div class="container">
			<h2 class="title">Podría interesarte</h2>
			<div class="more-news">
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="<?php echo get_template_directory_uri(); ?>/img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="<?php echo get_template_directory_uri(); ?>/img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="<?php echo get_template_directory_uri(); ?>/img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
			</div>
			<div class="text-center">
				<a href=""><span class="especial-btn red">MÁS NOTICIAS</span></a>
			</div>
		</div>
	</section>
  -->
<?php get_footer();  ?>