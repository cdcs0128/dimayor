<?php include("header.php");?>
	<section>
		<div class="principal-banner">
			<div class="slide">
				<img src="img/content/banner.jpg" alt="">
				<div class="cover-image-2 bg_2"></div>
				<div class="cover-image bg_1"></div>
				<div class="cover-text">
					<div class="container">
						<div class="table">
							<div class="text-image">
								<h2>La magia brilló en el partido de las leyendas FIFA - FCF</h2>
								<a href=""><span class="especial-btn white mt10">VER MÁS</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide">
				<img src="img/content/banner.jpg" alt="">
				<div class="cover-image-2 bg_2"></div>
				<div class="cover-image bg_1"></div>
				<div class="cover-text">
					<div class="container">
						<div class="table">
							<div class="text-image">
								<h2>La magia brilló en el partido de las leyendas FIFA - FCF</h2>
								<a href=""><span class="especial-btn white mt10">VER MÁS</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide">
				<img src="img/content/banner.jpg" alt="">
				<div class="cover-image-2 bg_2"></div>
				<div class="cover-image bg_1"></div>
				<div class="cover-text">
					<div class="container">
						<div class="table">
							<div class="text-image">
								<h2>La magia brilló en el partido de las leyendas FIFA - FCF</h2>
								<a href=""><span class="especial-btn white mt10">VER MÁS</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide">
				<img src="img/content/banner.jpg" alt="">
				<div class="cover-image-2 bg_2"></div>
				<div class="cover-image bg_1"></div>
				<div class="cover-text">
					<div class="container">
						<div class="table">
							<div class="text-image">
								<h2>La magia brilló en el partido de las leyendas FIFA - FCF</h2>
								<a href=""><span class="especial-btn white mt10">VER MÁS</span></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="app-dimayor">
			<div class="container ph">
				<iframe src="http://widgets.dayscript.com/clients/v01/?height=426&width=296&id=widget-ds-iframe-dim34E2G4RWS&theme=dimayor&opts=posiciones&rand=617.1527362894267#150952/posiciones/1175" frameborder="0"></iframe>
			</div>
		</div>
	</section>
	<section>
		<div class="section-multimedia bg_1">
			<div class="container">
				<div class="content-left lg_8 md_8 sm_8">
					<div class="principal-multimedia">
						<div class="content-image">
							<img src="img/content/content-01.jpg" alt="">
							<div class="cover-image">
								<div class="date-event">
									<p>10</p>
									<span>Oct</span>
								</div>
							</div>
						</div>
						<div class="description-image">
							<h3 class="hash_green">Categoria</h3>
							<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo ducimus, est perferendis sed excepturi.</p>
						</div>
					</div>
					<div class="box-multimedia lg_4 md_4 sm_4">
						<div class="top-box">
							<img src="img/content/content-01.jpg" alt="">
							<span class="cover-image"></span>
						</div>
						<div class="description-image">
							<h3 class="hash_green">Categoria</h3>
							<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
						</div>
					</div>
					<div class="box-multimedia lg_4 md_4 sm_4">
						<div class="top-box">
							<img src="img/content/content-01.jpg" alt="">
							<span class="cover-image"></span>
						</div>
						<div class="description-image">
							<h3 class="hash_green">Categoria</h3>
							<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
						</div>
					</div>
					<div class="box-multimedia lg_4 md_4 sm_4">
						<div class="top-box">
							<img src="img/content/content-01.jpg" alt="">
							<span class="cover-image"></span>
						</div>
						<div class="description-image">
							<h3 class="hash_green">Categoria</h3>
							<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
						</div>
					</div>
				</div>
				<div class="content-right lg_4 md_4 sm_4">
					<div class="box-advertising">
						<img src="img/content/advertising-01.png" alt="">
					</div>
					<div class="box-advertising">
						<img src="img/content/advertising-02.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="section-multimedia bg_2">
			<div class="container">
				<div class="content-left lg_8 md_8 sm_8">
					<div class="principal-multimedia option-2">
						<div class="content-image">
							<img src="img/content/content-01.jpg" alt="">
							<div class="cover-image">
								<div class="date-event">
									<p>10</p>
									<span>Oct</span>
								</div>
							</div>
						</div>
						<div class="description-image">
							<h3 class="hash_red">Categoria</h3>
							<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo ducimus, est perferendis sed excepturi.</p>
						</div>
					</div>
					<div class="box-multimedia option-2 lg_4 md_4 sm_4">
						<div class="top-box">
							<img src="img/content/content-01.jpg" alt="">
							<span class="cover-image"></span>
						</div>
						<div class="description-image">
							<h3 class="hash_red">Categoria</h3>
							<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
						</div>
					</div>
					<div class="box-multimedia option-2 lg_4 md_4 sm_4">
						<div class="top-box">
							<img src="img/content/content-01.jpg" alt="">
							<span class="cover-image"></span>
						</div>
						<div class="description-image">
							<h3 class="hash_red">Categoria</h3>
							<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
						</div>
					</div>
					<div class="box-multimedia option-2 lg_4 md_4 sm_4">
						<div class="top-box">
							<img src="img/content/content-01.jpg" alt="">
							<span class="cover-image"></span>
						</div>
						<div class="description-image">
							<h3 class="hash_red">Categoria</h3>
							<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</h4>
						</div>
					</div>
				</div>
				<div class="content-right lg_4 md_4 sm_4">
					<div class="box-first-multimedia">
						<img src="img/content/content-02.jpg" alt="">
						<div class="cover-image">
							<div class="text-multimedia">
								<h2>Lorem ipsum dolor sitamet, consectetur adipisicing elit.</h2>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque nisi mauris, viverra a dolor id, sodales aliquam metus.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

<?php include("footer.php");?>