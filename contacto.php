<?php include("header.php");?>
	<section class="module back-patch">
		<div class="container">
			<h1 class="title-small none-padding">Contacto</h1>
			<div class="contact-block">
				<div class="lg_8 md_8 ph10">
					<form action="" method="POST">
						<fieldset class="form-section">
							<label>Nombre</label><!--
							--><input type="text" class="form-ctrl">
						</fieldset>
						<fieldset class="form-section">
							<label>Correo</label><!--
							--><input type="email" class="form-ctrl">
						</fieldset>
						<fieldset class="form-section">
							<label>Teléfono</label><!--
							--><input type="text" class="form-ctrl">
						</fieldset>
						<fieldset class="form-section">
							<textarea name="" id="" placeholder="Mensaje" class="form-ctrl"></textarea>
						</fieldset>
						<fieldset class="form-section">
							<label class="check">
								<input type="checkbox">
								<i></i> <span><em>Acepto Políticas</em></span>
							</label>
						</fieldset>
						<fieldset class="form-section text-center">
							<button class="especial-btn red" type="submit">ENVIAR</button>
						</fieldset>
					</form>
				</div>
				<div class="lg_4 md_4 ph10">
					<div class="contact-info">
						<p><b>La Dimayor</b></p>
						<p>Teléfono <br>+57 91 398 43 00 <br>+57 90 232 18 09 <br><a href="mailto:atencionpublico@dimayor.com">atencionpublico@dimayor.com</a></p>
						<p>El servicio de atención al público está habilitado de lunes a domingo.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php include("footer.php");?>