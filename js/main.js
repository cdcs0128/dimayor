(function($) {

	jQuery(document).ready(function($) {

		// Galleries

		$('.gallery-clubes').slick({
			dots: false,
			arrows: true,
			infinite: true,
			speed: 300,
			slidesToShow: 14,
			autoplay: true,
			autoplaySpeed: 2000,
			swipeToSlide: true,
			adaptiveHeight: true,
			responsive: [
			    {
			      breakpoint: 800,
			      settings: {
			        slidesToShow: 10,
			        slidesToScroll: 1,
			      }
			    },
			    {
			      breakpoint: 500,
			      settings: {
			        slidesToShow: 6,
			        slidesToScroll: 1
			      }
			    }
			]
		});

		$('.principal-banner').slick({
			dots: true,
			arrows: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			autoplay: true,
			autoplaySpeed: 4000,
			adaptiveHeight: true
		});

		// $('.gallery-allides').slick({
		// 	dots: true,
		// 	arrows: true,
		// 	infinite: true,
		// 	speed: 300,
		// 	slidesToShow: 5,
		// 	autoplay: true,
		// 	autoplaySpeed: 3000,
		// 	adaptiveHeight: true,
		// 	responsive: [
		// 	    {
		// 	      breakpoint: 800,
		// 	      settings: {
		// 	        slidesToShow: 5,
		// 	        slidesToScroll: 1,
		// 	      }
		// 	    },
		// 	    {
		// 	      breakpoint: 500,
		// 	      settings: {
		// 	        slidesToShow: 2,
		// 	        slidesToScroll: 1
		// 	      }
		// 	    }
		// 	]
		// });


		$('.principal-header .ctrl-menu').on('click', function(event) {
			event.preventDefault();
			$(this).toggleClass('ctrl-menu-close');
			$('.principal-header nav').slideToggle(500,'swing');
		});

		// window.mobilecheck = function() {

		// 	$('.bot-header nav ul li.sub').on('click', function(event) {
		// 		event.preventDefault();
		// 		$(this).find('.submenu').stop().slideToggle(300);
		// 	});
		// };

		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			$('.bot-header nav ul li.sub').on('click', function(event) {
				event.preventDefault();
				$(this).find('.sub-menu').stop().slideToggle(300);
				// $(this).find('.submenu').stop().slideToggle(300);

			});
		}


		
		$(document).on('scroll', function(event) {
			event.preventDefault();

			// console.log($(this).scrollTop());
			/* Act on the event */
			if ($(this).scrollTop() > 60) {
				$('header .principal-header .top-header').slideUp(300);
			} else {
				$('header .principal-header .top-header').slideDown(300);
			}
		});

		$('header .principal-header .bot-header .btn-search').on('click', function(event) {
			event.preventDefault();

			$('header .principal-header .search-bar').stop().slideToggle(300);
		});

		// background sections
		if ($('[data-background]').length){
			$('[data-background]').each(function(index, el) {
				var source = $(el).attr('data-background');
			$(el).css({
					'background': 'url('+source+')',
					'background-size': 'cover'
				});
			});
		} 

		// Desplegables footer

		$('.Links-footer .box-link a').on('click', function () {
			$(this).next('ul').slideToggle(300);
			$(this).toggleClass('active');
		});

		$('.pager .page-numbers.prev').html(' '); 
		$('.pager .page-numbers.next').html(' '); 

		// clic menu submenu

		$('li.menu-item-has-children').on('click', function () {
			$(this).find('.sub-menu').slideToggle(300);
			$(this).toggleClass('sub-active');
			$(this).siblings().removeClass('sub-active')
			$(this).siblings().find('.sub-menu').slideUp(300);
		});

		// $('li.sub').on('click', function () {
		// 	$(this).find('.submenu').slideToggle(300);
		// 	$(this).toggleClass('sub-active');
		// 	$(this).siblings().removeClass('sub-active')
		// 	$(this).siblings().find('.submenu').slideUp(300);
		// });

		
	});
	
})(jQuery);