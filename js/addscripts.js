(function($){
	jQuery(document).ready(function() {
		// small clubes slider
			$('.small-thumb-gallery .slide').each(function(ind,ele){
				var number_text = (ind+1)+'/'+$('.small-thumb-gallery .slide').length;  
				$(this).find('.selector .number').html(number_text);
			});
			$('.small-thumb-gallery img').each(function(ind,ele){
				var src = $(this).attr('src');  
				$('.small-thumb-selector').append('<div class="slide"><img src="'+src+'" alt=""></div>');
			});
			$('.small-thumb-gallery').slick({
				slidesToShow: 1,
				slidesToScroll: 1,
				arrows: false,
				fade: true,
				asNavFor: '.small-thumb-selector'
			});
			$('.small-thumb-gallery .next').on('click', function(event) {
				event.preventDefault();
				$('.small-thumb-gallery').slick('slickNext');
			});
			$('.small-thumb-gallery .prev').on('click', function(event) {
				event.preventDefault();
				$('.small-thumb-gallery').slick('slickPrev');
			});
			$('.small-thumb-selector').slick({
				slidesToShow: 6,
				slidesToScroll: 1,
				asNavFor: '.small-thumb-gallery',
				dots: false,
				focusOnSelect: true,
				swipeToSlide: true,
				responsive: [
					{
						breakpoint: 900,
						settings: {
							slidesToShow: 4,
							slidesToScroll: 1,
							infinite: true,
							autoplay: true,
							autoplaySpeed: 2000
						}
					}
				]
			});
		// tabs selector
			$('.tabs-selection .tab-ctrl').on('click',function(event){
				event.preventDefault();
				$(this).siblings('.tab-ctrl').removeClass('active');
				$(this).addClass('active');
				var focusTab = $(this).parents('.tabs-selection').attr('data-tabs');
				var ind = $(this).index();
				var $parentTab = $('.tabs-content[data-tabs="'+focusTab+'"]')[0];
				$($parentTab).find('.tab-block').removeAttr('style').removeClass('active');
				$($parentTab).find('.tab-block:eq('+ind+')').stop().fadeIn(300,function(){
					$(this).addClass('active').removeAttr('style');
				});
			});
	});
})(jQuery)