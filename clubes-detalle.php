<?php include("header.php");?>
	<section class="module back-patch">
		<div class="container">
			<div class="team-banner">
				<div class="info-section">
					<div class="table">
						<div class="table-cell">
							<div class="shield-team"><img src="img/clubes/once-caldas/once-caldas-logo.png" alt=""></div>
							<div class="info-team">
								<h1 class="team-name">Once Caldas</h1>
								<p><a href="http://www.oncecaldas.com"><span>www.oncecaldas.com</span></a></p>
								<p>En las redes</p>
								<ul class="social">
									<li><a href=""><span class="icon icon-facebook"></span></a></li>
									<li><a href=""><span class="icon icon-twitter"></span></a></li>
									<li><a href=""><span class="icon icon-instagram"></span></a></li>
								</ul>
								<p>Torneos:
									<span class="icon icon-star-fill"></span>
									<span class="icon icon-star-fill"></span>
									<span class="icon icon-star-fill"></span>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="image-section">
					<img src="img/clubes/once-caldas/once-caldas.png" alt="">
				</div>
			</div>
			<div class="tabs-selection" data-tabs="tab">
				<div class="tab-ctrl especial-btn red active">Sobre el club</div><!--
				--><div class="tab-ctrl especial-btn blue">Cuerpo técnico y jugadores</div>
			</div>
			<div class="tabs-content" data-tabs="tab">
				<div class="tab-block active">
					<div class="row">
						<div class="table table-spacing">
							<div class="info-block">
								<div class="text">
									<p><b>Fundación:</b> 15 / 11 / 1995</p>
									<p><b>Presidente:</b> Nelson Soto</p>
									<p><b>DT:</b> Umberto</p>
								</div>
							</div>
							<div class="info-block">
								<div class="text">
									<p><b>Razón social:</b> 15 / 11 / 1995</p>
									<p><b>Ciudad:</b> Monteria</p>
									<p><b>Estadio:</b> Municipal de Montería</p>
									<p><b>Sede:</b> Calle 140 #10-67</p>
								</div>
							</div>
							<div class="info-block">
								<div class="text">
									<p><b>Contacto:</b></p>
									<p>(1) 7433941 - FAX 7433941</p>
									<p><a href="mailto:contacto@oncecaldas.co">contacto@oncecaldas.co</a></p>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix">
						<div class="left-block pr20">
							<div class="small-thumb-gallery">
								<div class="slide">
									<img src="img/clubes/once-caldas/01.png" alt="">
									<div class="block-text">
										<div class="selector">
											<span class="prev"><i class="icon-left"></i></span>
											<span class="number"></span>
											<span class="next"><i class="icon-right"></i></span>
										</div>
										<div class="info">
											<p><b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Tenetur sed, deserunt vero. Dolorum perspiciatis quod assumenda ipsa at provident voluptate magni dicta, tenetur perferendis voluptatem fugit ipsam quos, omnis, ad.</p>
										</div>
									</div>
								</div>
								<div class="slide">
									<img src="img/clubes/once-caldas/01.png" alt="">
									<div class="block-text">
										<div class="selector">
											<span class="prev"><i class="icon-left"></i></span>
											<span class="number"></span>
											<span class="next"><i class="icon-right"></i></span>
										</div>
										<div class="info">
											<p><b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Tenetur sed, deserunt vero. Dolorum perspiciatis quod assumenda ipsa at provident voluptate magni dicta, tenetur perferendis voluptatem fugit ipsam quos, omnis, ad.</p>
										</div>
									</div>
								</div>
								<div class="slide">
									<img src="img/clubes/once-caldas/01.png" alt="">
									<div class="block-text">
										<div class="selector">
											<span class="prev"><i class="icon-left"></i></span>
											<span class="number"></span>
											<span class="next"><i class="icon-right"></i></span>
										</div>
										<div class="info">
											<p><b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Tenetur sed, deserunt vero. Dolorum perspiciatis quod assumenda ipsa at provident voluptate magni dicta, tenetur perferendis voluptatem fugit ipsam quos, omnis, ad.</p>
										</div>
									</div>
								</div>
								<div class="slide">
									<img src="img/clubes/once-caldas/01.png" alt="">
									<div class="block-text">
										<div class="selector">
											<span class="prev"><i class="icon-left"></i></span>
											<span class="number"></span>
											<span class="next"><i class="icon-right"></i></span>
										</div>
										<div class="info">
											<p><b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Tenetur sed, deserunt vero. Dolorum perspiciatis quod assumenda ipsa at provident voluptate magni dicta, tenetur perferendis voluptatem fugit ipsam quos, omnis, ad.</p>
										</div>
									</div>
								</div>
								<div class="slide">
									<img src="img/clubes/once-caldas/01.png" alt="">
									<div class="block-text">
										<div class="selector">
											<span class="prev"><i class="icon-left"></i></span>
											<span class="number"></span>
											<span class="next"><i class="icon-right"></i></span>
										</div>
										<div class="info">
											<p><b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Tenetur sed, deserunt vero. Dolorum perspiciatis quod assumenda ipsa at provident voluptate magni dicta, tenetur perferendis voluptatem fugit ipsam quos, omnis, ad.</p>
										</div>
									</div>
								</div>
								<div class="slide">
									<img src="img/clubes/once-caldas/01.png" alt="">
									<div class="block-text">
										<div class="selector">
											<span class="prev"><i class="icon-left"></i></span>
											<span class="number"></span>
											<span class="next"><i class="icon-right"></i></span>
										</div>
										<div class="info">
											<p><b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Tenetur sed, deserunt vero. Dolorum perspiciatis quod assumenda ipsa at provident voluptate magni dicta, tenetur perferendis voluptatem fugit ipsam quos, omnis, ad.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="small-thumb-selector"></div>
						</div>
						<div class="advertising-block">
							<div class="ranking-widget">
								<script type="text/javascript">
									(function( ) {
										var p = {
											height: 426,
											width: 296,
											id: 'widget-ds-iframe-dim34E2G4RWS',
											theme: 'dimayor',
											opts: 'posiciones'
										};
										document.write(['<iframe id="', p.id,'" style="','border:0;','width:',(p.width?Math.max(280,p.width)+'px;':'100%;'),'height:',Math.max(400,p.height),'px"',' scrolling="no"',' frameborder="0"',' height="',Math.max(400,p.height),'" width="',(p.width?Math.max(280,p.width):'100%'),'" allowtransparency="true"', ' src="//widgets.dayscript.com/clients/v01/?',(function(){var a = [];p.height=(p.height?Math.max(400,p.height):'');p.width=(p.width?Math.max(280,p.width):'');p.rand=(Math.random()*1000);for(var s in p){a.push(s+'='+p[s])};return a.join('&')})(),'"><\/iframe>'].join(''));
									})();
								</script>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-block">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum inventore, ullam corrupti magnam dolores, molestias necessitatibus dignissimos possimus sunt vitae quae at repellendus nobis ipsa, ad suscipit fuga animi cupiditate.</p>
				</div>
			</div>
		</div>
	</section>
	<section class="module backgray">
		<div class="container">
			<h2 class="title">Podría interesarte</h2>
			<div class="more-news">
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
			</div>
			<div class="text-center">
				<a href=""><span class="especial-btn red">MÁS NOTICIAS</span></a>
			</div>
		</div>
	</section>
<?php include("footer.php");?>