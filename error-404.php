<?php include("header.php");?>
	<section>
		
		<div class="container-error">
			<div class="container">
				<div class="text-error">
					<h1 class="title-error">ERROR 404</h1>
					<h2>PÁGINA NO ENCONTRADA</h2>
					<p>Lo sentimos, la página que buscas no existe o no se puede encontrar</p>
				</div>
			</div>
		</div>
	</section>
<?php include("footer.php");?>