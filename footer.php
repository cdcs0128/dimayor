		
	</div>
	<footer>
		<div class="container">
			<div class="top-footer">
				<div class="aliados-dimayor lg_6 md_6 sm_6">
					<h2 class="mr20">Aliados Dimayor</h2>
					<div class="allies gallery-allides">
						<div class="slide-aliides ">
							<a href=""><img src="img/content/sponsor-01.png" alt=""></a>
						</div>
						<div class="slide-aliides">
							<a href=""><img src="img/content/sponsor-02.png" alt=""></a>
						</div>
						<div class="slide-aliides">
							<a href=""><img src="img/content/sponsor-03.png" alt=""></a>
						</div>
						<div class="slide-aliides">
							<a href=""><img src="img/content/sponsor-05.png" alt=""></a>
						</div>
						<div class="slide-aliides">
							<a href=""><img src="img/content/sponsor-04.png" alt=""></a>
						</div>
					</div>
				</div>
				<div class="aliados-dimayor lg_6 md_6 sm_6">
					<h2>Links de interés</h2>
					<div class="allies">
						<div class="slide-aliides">
							<a href=""><img class="wimg" src="img/content/links-01.png" alt=""></a>
						</div>
						<div class="slide-aliides">
							<a href=""><img class="wimg" src="img/content/links-02.png" alt=""></a>
						</div>
						<div class="slide-aliides">
							<a href=""><img class="wimg" src="img/content/links-01.png" alt=""></a>
						</div>
					</div>
				</div>
				<div class="Links-footer lg_12 md_12 sm_12">
					<div class="box-link">
						<h2>La Dimayor</h2>
						<ul>
							<li><a href="" target="_blank">Reseña Historica</a></li>
							<li><a href="" target="_blank">Organigrama</a></li>
							<li><a href="" target="_blank">Cuadro de Campeones</a></li>
							<li><a href="" target="_blank">Centro de medios</a></li>
							<li><a href="" target="_blank">Habeas data</a></li>
						</ul>
					</div>
					<div class="box-link">
						<h2>Reglamentaciones y códigos</h2>
						<ul>
							<li><a href="" target="_blank">Reglamentos Campeonatos</a></li>
							<li><a href="" target="_blank">Código Disciplinario</a></li>
							<li><a href="" target="_blank">Estatuto del Jugador</a></li>
							<li><a href="" target="_blank">Código Antidopaje</a></li>
							<li><a href="" target="_blank">Sustacias Prohibidas</a></li>
							<li><a href="" target="_blank">Protocolo Medios Comunicación</a></li>
							<li><a href="" target="_blank">Resoluciones y sanciones</a></li>
						</ul>
					</div>
					<div class="box-link">
						<h2>Campeonatos</h2>
						<ul>
							<li><a href="" target="_blank">Noticias</a></li>
							<li><a href="" target="_blank">Liga Águila</a></li>
							<li><a href="" target="_blank">Torneo Águila</a></li>
							<li><a href="" target="_blank">Copa Águila</a></li>
							<li><a href="" target="_blank">Superliga Águila</a></li>
							<li><a href="" target="_blank">Rincón Histórico</a></li>
						</ul>
					</div>
					<div class="box-link">
						<h2>Resoluciones</h2>
						<ul>
							<li><a href="" target="_blank">CD del Campeonato</a></li>
							<li><a href="" target="_blank">CD de la Dimayor</a></li>
							<li><a href="" target="_blank">Comisión Estatuto</a></li>
							<li><a href="" target="_blank">Jugador</a></li>
						</ul>
					</div>
					<div class="box-link">
						<h2>Clubes</h2>
						<ul>
							<li><a href="" target="_blank">Liga Águila</a></li>
							<li><a href="" target="_blank">Torneo Águila</a></li>
						</ul>
					</div>
				</div>
				<!-- <div class="Links-footer lg_12 md_12 sm_12">
					<div class="box-link">
						<p><b><a href="" target="_blank">La Dimayor</a></b></p>
						<p><a href="" target="_blank">Reseña Historica</a></p>
						<p><a href="" target="_blank">Organigrama</a></p>
						<p><a href="" target="_blank">Cuadro de Campeones</a></p>
						<p><a href="" target="_blank">Centro de medios</a></p>
						<p><a href="" target="_blank">Habeas data</a></p>
					</div>
					<div class="box-link">
						<p><b><a href="" target="_blank">Reglamentaciones y códigos</a></b></p>
						<p><a href="" target="_blank">Reglamentos Campeonatos</a></p>
						<p><a href="" target="_blank">Código Disciplinario</a></p>
						<p><a href="" target="_blank">Estatuto del Jugador</a></p>
						<p><a href="" target="_blank">Código Antidopaje</a></p>
						<p><a href="" target="_blank">Sustacias Prohibidas</a></p>
						<p><a href="" target="_blank">Protocolo Medios Comunicación</a></p>
						<p><a href="" target="_blank">Resoluciones y sanciones</a></p>
					</div>
					<div class="box-link">
						<p><b><a href="" target="_blank">Campeonatos</a></b></p>
						<p><a href="" target="_blank">Noticias</a></p>
						<p><a href="" target="_blank">Liga Águila</a></p>
						<p><a href="" target="_blank">Torneo Águila</a></p>
						<p><a href="" target="_blank">Copa Águila</a></p>
						<p><a href="" target="_blank">Superliga Águila</a></p>
						<p><a href="" target="_blank">Rincón Histórico</a></p>
					</div>
					<div class="box-link">
						<p><b><a href="" target="_blank">Resoluciones</a></b></p>
						<p><a href="" target="_blank">CD del Campeonato</a></p>
						<p><a href="" target="_blank">CD de la Dimayor</a></p>
						<p><a href="" target="_blank">Comisión Estatuto</a></p>
						<p><a href="" target="_blank">Jugador</a></p>
					</div>
					<div class="box-link">
						<p><b><a href="" target="_blank">Clubes</a></b></p>
						<p><a href="" target="_blank">Liga Águila</a></p>
						<p><a href="" target="_blank">Torneo Águila</a></p>
					</div>
				</div> -->
			</div>
		</div>
		<div class="bottom-footer">
			<div class="container">
				<div class="left">
					<ul>
						<li>Copyright © 2013 DIMAYOR </li>
						<li>|</li>
						<li>Carrera 15 No. 32 - 83 PBX: 288 61 66</li>
						<li>|</li>
						<li>Fax: 288 08 92 / 288 32 62</li>
						<li>|</li>
						<li>Bogotá D.C. · Colombia</li>
						<li>|</li>
						<li><a href="">Contáctenos</a></li>
					</ul>
				</div>
				<div class="right">
					<a href=""><h3>Powered by: Bluecel</h3></a>
				</div>
			</div>
		</div>
	</footer>
	
	<script src="js/app.min.js"></script>
	<script src="js/addscripts.js"></script>
</body>
</html>