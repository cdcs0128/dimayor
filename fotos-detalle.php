<?php include("header.php");?>
	<section class="module back-patch">
		<div class="container">
			
			<div class="tabs-content" data-tabs="tab">
				<div class="tab-block active">
					<article class="main-article">
						<header class="article-header">
							<div class="breadcrumb-block">
								<a href=""><span class="breadcrumb">Fotos</span></a>
							</div>
							<h1 class="title">Partido de hoy 12 de octubre de la Copa Águila</h1>
							<div class="share">Compartir <a href=""><span class="icon icon-facebook"></span></a> <a href=""><span class="icon icon-twitter"></span></a></div>
						</header>
						<div class="clearfix">
							<div class="small-thumb-gallery">
								<div class="slide">
									<img src="img/clubes/once-caldas/01.png" alt="">
									<div class="block-text">
										<div class="selector">
											<span class="prev"><i class="icon-left"></i></span>
											<span class="number"></span>
											<span class="next"><i class="icon-right"></i></span>
										</div>
										<div class="info">
											<p><b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Tenetur sed, deserunt vero. Dolorum perspiciatis quod assumenda ipsa at provident voluptate magni dicta, tenetur perferendis voluptatem fugit ipsam quos, omnis, ad.</p>
										</div>
									</div>
								</div>
								<div class="slide">
									<img src="img/clubes/once-caldas/01.png" alt="">
									<div class="block-text">
										<div class="selector">
											<span class="prev"><i class="icon-left"></i></span>
											<span class="number"></span>
											<span class="next"><i class="icon-right"></i></span>
										</div>
										<div class="info">
											<p><b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Tenetur sed, deserunt vero. Dolorum perspiciatis quod assumenda ipsa at provident voluptate magni dicta, tenetur perferendis voluptatem fugit ipsam quos, omnis, ad.</p>
										</div>
									</div>
								</div>
								<div class="slide">
									<img src="img/clubes/once-caldas/01.png" alt="">
									<div class="block-text">
										<div class="selector">
											<span class="prev"><i class="icon-left"></i></span>
											<span class="number"></span>
											<span class="next"><i class="icon-right"></i></span>
										</div>
										<div class="info">
											<p><b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Tenetur sed, deserunt vero. Dolorum perspiciatis quod assumenda ipsa at provident voluptate magni dicta, tenetur perferendis voluptatem fugit ipsam quos, omnis, ad.</p>
										</div>
									</div>
								</div>
								<div class="slide">
									<img src="img/clubes/once-caldas/01.png" alt="">
									<div class="block-text">
										<div class="selector">
											<span class="prev"><i class="icon-left"></i></span>
											<span class="number"></span>
											<span class="next"><i class="icon-right"></i></span>
										</div>
										<div class="info">
											<p><b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Tenetur sed, deserunt vero. Dolorum perspiciatis quod assumenda ipsa at provident voluptate magni dicta, tenetur perferendis voluptatem fugit ipsam quos, omnis, ad.</p>
										</div>
									</div>
								</div>
								<div class="slide">
									<img src="img/clubes/once-caldas/01.png" alt="">
									<div class="block-text">
										<div class="selector">
											<span class="prev"><i class="icon-left"></i></span>
											<span class="number"></span>
											<span class="next"><i class="icon-right"></i></span>
										</div>
										<div class="info">
											<p><b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Tenetur sed, deserunt vero. Dolorum perspiciatis quod assumenda ipsa at provident voluptate magni dicta, tenetur perferendis voluptatem fugit ipsam quos, omnis, ad.</p>
										</div>
									</div>
								</div>
								<div class="slide">
									<img src="img/clubes/once-caldas/01.png" alt="">
									<div class="block-text">
										<div class="selector">
											<span class="prev"><i class="icon-left"></i></span>
											<span class="number"></span>
											<span class="next"><i class="icon-right"></i></span>
										</div>
										<div class="info">
											<p><b>Lorem ipsum dolor sit amet</b>, consectetur adipisicing elit. Tenetur sed, deserunt vero. Dolorum perspiciatis quod assumenda ipsa at provident voluptate magni dicta, tenetur perferendis voluptatem fugit ipsam quos, omnis, ad.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="small-thumb-selector"></div>
							<div class="box-description">
								<p>El delantero de Jaguares de Córdoba, Martín “Torito” Arzuaga afirmó que las malas decisiones arbitrales que ha sufrido Jaguares de Montería en los últimos partidos están golpeando las expectativas de ascenso del club en la Liga I-2015.</p>
								<p>La afirmación la hizo el atacante “felino” antes de que viajará a Bogotá, para el encuentro contra Santa Fe, por la 17 fecha de Liga Águila.</p>
								<p>El delantero del plantel “celeste” de Becerril (Cesar), que se ha convertido en goleador y figura del club, pidió a los jueces un mayor equilibrio en el terreno de juego.</p>
								
								<div class="share right">Comparte este contenido en <a href=""><span class="icon icon-facebook"></span></a> <a href=""><span class="icon icon-twitter"></span></a></div>
							</div>
						</div>
					</article>
				</div>
				<div class="tab-block">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatum inventore, ullam corrupti magnam dolores, molestias necessitatibus dignissimos possimus sunt vitae quae at repellendus nobis ipsa, ad suscipit fuga animi cupiditate.</p>
				</div>
			</div>
		</div>
	</section>
	<section class="module backgray">
		<div class="container">
			<h2 class="title">Podría interesarte</h2>
			<div class="more-news">
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
			</div>
			<div class="text-center">
				<a href=""><span class="especial-btn red">MÁS NOTICIAS</span></a>
			</div>
		</div>
	</section>
<?php include("footer.php");?>