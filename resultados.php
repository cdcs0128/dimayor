<?php include("header.php");?>
	<section class="module backgray none-padding">
		<div class="container">
			<div class="search-result">
				<form action="">
					<input type="text" placeholder="Millonarios">
				</form>
			</div>
		</div>
	</section>
	<section class="module back-patch">
		<div class="container">
			<h1 class="title-result none-padding">Resultados de la busqueda: <span>Millonarios</span></h1>
			<div class="more-news">
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
			</div>
			<ul class="pager">
				<li class="prev"><a href=""></a></li>
				<li class="active"><a href="">1</a></li>
				<li><a href="">2</a></li>
				<li><a href="">3</a></li>
				<li><a href="">4</a></li>
				<li><a href="">5</a></li>
				<li><a href="">6</a></li>
				<li><a href="">7</a></li>
				<li class="next"><a href=""></a></li>
			</ul>
		</div>
	</section>
<?php include("footer.php");?>