<!DOCTYPE html>
<html lang="es" xml:lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Dimayor</title>
	<meta name="keywords" content="">
	<meta name="author" content="">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0, minimum-scale=1.0">
	<link rel="icon" href="favicon.ico">
	<link rel="stylesheet" href="css/main.css">
	<link rel="stylesheet" href="css/other-styles.css">
</head>
<body>
	<header>
		<div class="principal-header">
			<div class="top-header">
				<div class="container clearfix ph">
					<div class="text-clubes lg_1 md_1 sm_1">
						<p>Clubes</p>
					</div>
					<div class="gallery-clubes lg_11 md_11 sm_11">
						<div class="slides">
							<img src="img/clubes/club-color/club-01.png" alt="">
							<div class="label-club">
								<span>Club 01</span>
							</div>
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-02.png" alt="">
							<div class="label-club">
								<span>Club 01</span>
							</div>
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-03.png" alt="">
							<div class="label-club">
								<span>Club 01</span>
							</div>
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-04.png" alt="">
							<div class="label-club">
								<span>Club 01</span>
							</div>
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-05.png" alt="">
							<div class="label-club">
								<span>Club 01</span>
							</div>
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-06.png" alt="">
							<div class="label-club">
								<span>Club 01</span>
							</div>
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-07.png" alt="">
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-08.png" alt="">
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-09.png" alt="">
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-10.png" alt="">
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-11.png" alt="">
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-12.png" alt="">
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-13.png" alt="">
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-14.png" alt="">
						</div>
						<div class="slides">
							<img src="img/clubes/club-color/club-15.png" alt="">
						</div>
					</div>
				</div>
			</div>
			<div class="bot-header">
				<div class="container">
					<div class="ctrl-menu">
						<span class="span1"></span>
						<span class="span2"></span>
						<span class="span3"></span>
					</div>
					<div class="principal-logo">
						<a href="index.php"><img src="img/logo.png" alt=""></a>
					</div>
					<nav>
						<ul>
							<li class="logo"><a href="index.php"><span></span></a><img src="img/logo.png" alt=""></li>
							<li class="sub" data-focus="submenu-clubes">Clubes
								<div class="submenu clubes">
									<div class="container">
										<ul>
											<li><a href=""><span></span></a>Liga Águila</li>
											<li><a href=""><span></span></a>Torneo Águila</li>
											<li><a href=""><span></span></a>Cuadro campeones</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="sub" data-focus="submenu-campeones">Campeonatos
								<div class="submenu campeonatos">
									<div class="container">
										<ul>
											<li><a href=""><span></span></a>Liga Águila</li>
											<li><a href=""><span></span></a>Torneo Águila</li>
											<li><a href=""><span></span></a>Copa Águila</li>
											<li><a href=""><span></span></a>Super Liga Águila</li>
										</ul>
									</div>
								</div>
							</li>
							<li><a href=""><span></span></a>Calendario</li>

							<li class="sub">Noticias
								<div class="submenu noticias">
									<div class="container">
										<ul>
											<li><a href=""><span></span></a>Por los clubes</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="sub">Resoluciones
								<div class="submenu resoluciones">
									<div class="container">
										<ul>
											<li><a href=""><span></span></a>CD del campeonatos</li>
											<li><a href=""><span></span></a>CD cd la Dimayor</li>
											<li><a href=""><span></span></a>Comisión Estatuto Jugador</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="sub">La Dimayor
								<div class="submenu dimayor">
									<div class="container">
										<ul>
											<li><a href=""><span></span></a>Rincón historico</li>
											<li><a href=""><span></span></a>Opiniones</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="sub">Somos Familia
								<div class="submenu familia">
									<div class="container">
										<ul>
											<li><a href=""><span></span></a>Liga Águila</li>
											<li><a href=""><span></span></a>Torneo Águila</li>
											<li><a href=""><span></span></a>Copa Águila</li>
											<li><a href=""><span></span></a>Super Liga Águila</li>
										</ul>
									</div>
								</div>
							</li>
						</ul>
					</nav>
					<div class="btn-search">
						<span class="icon icon-search"></span>
					</div>
				</div>
			</div>
			<div class="search-bar">
				<div class="container">
					<span class="icon icon-search"></span>
					<form action="">
						<input type="text" placeholder="¿Qué buscas?">
					</form>
				</div>
			</div>
		</div>
	</header>
	<div class="wrapper">