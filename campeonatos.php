<?php include("header.php");?>
	<section class="module back-patch">
		<div class="container">
			<h1 class="title-small">Sistema de juego Liga Águila</h1>
			<div class="share">Compartir <a href=""><span class="icon icon-facebook"></span></a> <a href=""><span class="icon icon-twitter"></span></a></div>
			<figure class="championship-img">
				<img src="img/campeonatos/ligaaguila.png" alt="">
			</figure>
			<div class="text-center">
				<div class="download-btn red"><a href=""><span class="name">Calendario<br><b>Liga Águila</b></span><span class="download">Descargar <i class="icon icon-download"></i></span></a></div>
				<div class="download-btn blue"><a href=""><span class="name">Reglamento<br><b>Liga Águila</b></span><span class="download">Descargar <i class="icon icon-download"></i></span></a></div>
			</div>
		</div>
	</section>
	<section class="module backgray">
		<div class="container">
			<h2 class="title">Podría interesarte</h2>
			<div class="more-news">
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
				<div class="news-block">
					<a href=""><span class="link"></span></a>
					<figure class="cover-img">
						<img src="img/news/img01.png" alt="">
						<time>27 de Julio de 2016</time>
					</figure>
					<div class="cover-text">
						<ul class="tag">
							<li>Categoria</li>
						</ul>
						<p><b>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</b></p>
					</div>
				</div>
			</div>
			<div class="text-center">
				<a href=""><span class="especial-btn red">MÁS NOTICIAS</span></a>
			</div>
		</div>
	</section>
<?php include("footer.php");?>